#include "Cars.h"
#include <iostream>
#include <string.h>

using namespace std;

int main()
{
    Car c("Volkswagen","Golf", "SB77HHH");
    DetaliiCar d(1595, 5, "ABC", "Motorina");
    Proprietar p("Nanu", "Paul", "60.13.1997", "Sb555555");
    Service s("Flanse amortizoare", "Nelu", "Nerezolvat");

    c.ConsoleWrite();
    d.ConsoleWrite();
    p.ConsoleWrite();
    s.ConsoleWrite();
    cout << endl;

    s.updateStatus("Rezolvat");
    p.updateNume("Paul");
    p.updatePrenume("Nanu");
    p.updateSerieBuletin("Vl55555");
    d.AdaugareCombustibil("Benzina");

    c.ConsoleWrite();
    d.ConsoleWrite();
    p.ConsoleWrite();
    s.ConsoleWrite();
    cout << endl;
}
