#pragma once
#include <string.h>
#include <iostream>

using namespace std;

class HasConsoleOperations {
	virtual void ConsoleWrite() = 0;
	virtual void ConsoleRead() = 0;
};

class Car :HasConsoleOperations
{
	char Marca[20];
	char Model[20];
	char Numar[10];
public:
	//Constructor cu parametrii
	Car(const char marca[20], const char model[20], const char numar[10])
	{
		strcpy_s(Marca, marca);
		strcpy_s(Model, model);
		strcpy_s(Numar, numar);
	}
	//Constructor fara parametrii
	Car()
	{
		strcpy_s(Marca, " ");
		strcpy_s(Model, " ");
		strcpy_s(Numar, " ");
	}
	//Scriere in consola
	virtual void ConsoleWrite()
	{
		cout << Marca << " " << Model << " " << Numar << endl;
	}
	//Citire din consola
	virtual void ConsoleRead()
	{
		char marca[20];
		char model[20];
		char numar[10];
		cout << "Marca ";
		cin >> marca;
		cout << "Model ";
		cin >> model;
		cout << "Numar ";
		cin >> numar;
		strcpy_s(Marca, marca);
		strcpy_s(Model, model);
		strcpy_s(Numar, numar);
	}
};

class DetaliiCar :Car
{
	int Capacitate;
	int Locuri;
	char Serie[20];
	char Combustibil[10];
public:
	//Constructor cu parametrii
	DetaliiCar(const int capacitate, const int locuri, const char serie[20], const char combustibil[10])
	{
		Capacitate = capacitate;
		Locuri = locuri;
		strcpy_s(Serie, serie);
		strcpy_s(Combustibil, combustibil);
	}
	//Constructor fara parametrii
	DetaliiCar()
	{
		Capacitate = 0;
		Locuri = 0;
		strcpy_s(Serie, " ");
		strcpy_s(Combustibil, " ");
	}
	//Scriere in consola
	virtual void ConsoleWrite()
	{
		cout<<Capacitate<<" "<<Locuri<<" "<<Serie<<" "<<Combustibil<<endl;
	}
	//Citire din consola
	virtual void ConsoleRead()
	{
		int capacitate;
		int locuri;
		char serie[20];
		char combustibil[20];
		cout << "Capacitate ";
		cin >> capacitate;
		cout << "Locuri ";
		cin >> locuri;
		cout << "Serie ";
		cin >> serie;
		cout << "Combustibil ";
		cin >> combustibil;
		Capacitate = capacitate;
		Locuri = locuri;
		strcpy_s(Serie, serie);
		strcpy_s(Combustibil, combustibil);
	}
	//Actualizare combustibil
	void AdaugareCombustibil(const char combustibil[20])
	{
		if (strcmp(combustibil, "Benzina") == 0 || strcmp(combustibil, "Motorina") == 0 || strcmp(combustibil, "Hybrid") == 0 || strcmp(combustibil, "Electric") == 0 || strcmp(combustibil, "gpl") == 0)
			strcpy_s(Combustibil, combustibil);
		else
			cout << "Nu exista combustibilul introdus " << endl;
	}
};

class Proprietar :Car
{
	char Nume[20];
	char Prenume[20];
	char DataNasterii[20];
	char SerieBuletin[10];
public:
	//constructor cu parametrii
	Proprietar(const char nume[20], const char prenume[20], const char dataNasterii[20], const char serieBuletin[10])
	{
		strcpy_s(Nume, nume);
		strcpy_s(Prenume, prenume);
		strcpy_s(DataNasterii, dataNasterii);
		strcpy_s(SerieBuletin, serieBuletin);
	}
	//constructor fara parametrii
	Proprietar()
	{
		strcpy_s(Nume, " ");
		strcpy_s(Prenume, " ");
		strcpy_s(DataNasterii, " ");
		strcpy_s(SerieBuletin, " ");
	}
	//Actualizare Nume
	void updateNume(const char nume[20])
	{
		strcpy_s(Nume, nume);
	}
	//Actualizare Prenume
	void updatePrenume(const char prenume[20])
	{
		strcpy_s(Prenume, prenume);
	}
	//Actualizare serie de buletin
	void updateSerieBuletin(const char serieBuletin[10])
	{
		strcpy_s(SerieBuletin, serieBuletin);
	}
	//Scriere in consola
	virtual void ConsoleWrite()
	{
		cout << Nume << " " << Prenume << " " << DataNasterii << " " << SerieBuletin << endl;
	}
	//Citire din consola
	virtual void ConsoleRead()
	{

		char nume[20];
		char prenume[20];
		char dataNasterii[20];
		char serieBuletin[10];
		cout << "Nume ";
		cin >> nume;
		cout << "Prenume ";
		cin >> prenume;
		cout << "DataNasterii ";
		cin >> dataNasterii;
		cout << "SerieBuletin ";
		cin >> serieBuletin;
		strcpy_s(Nume, nume);
		strcpy_s(Prenume, prenume);
		strcpy_s(DataNasterii, dataNasterii);
		strcpy_s(SerieBuletin, serieBuletin);
	}
};

class Service :DetaliiCar
{
	char Problema[50];
	char Mecanic[30];
	char Status[20];
public:
	//constructor cu parametrii
	Service(const char problema[50], const char mecanic[30], const char status[20])
	{
		strcpy_s(Problema, problema);
		strcpy_s(Mecanic, mecanic);
		strcpy_s(Status, status);
	}
	//constructor fara parametrii
	Service()
	{
		strcpy_s(Problema, " ");
		strcpy_s(Mecanic, " ");
		strcpy_s(Status, " ");
	}
	//Actualizare status
	void updateStatus(const char status[10])
	{
		strcpy_s(Status, status);
	}
	//Scriere in consola
	virtual void ConsoleWrite()
	{
		cout << Problema << " " << Mecanic << " " << Status<<endl;
	}
	//Citire din consola
	virtual void ConsoleRead()
	{

		char problema[20];
		char mecanic[20];
		char status[20];
		cout << "Problema ";
		cin >> problema;
		cout << "Mecanic ";
		cin >> mecanic;
		cout << "Status ";
		cin >> status;
		strcpy_s(Problema, problema);
		strcpy_s(Mecanic, mecanic);
		strcpy_s(Status, status);
	}
};